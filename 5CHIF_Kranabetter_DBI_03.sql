-- fan trap

drop table student;
drop table raum;
drop table semester;

create table raum (
  raum_id integer not null primary key,
  raum_nr varchar2(50) null,
  sem_id integer null
);

create table student (
  stud_id integer not null primary key,
  stud_name varchar2(50) null,
  sem_id integer null
);

create table semester (
  sem_id integer not null primary key,
  sem_bez varchar2(50) null
);

alter table raum add foreign key (sem_id) references semester(sem_id);
alter table student add foreign key (sem_id) references semester(sem_id);

insert into semester(sem_id, sem_bez) values(1, 'S1');
insert into semester(sem_id, sem_bez) values(2, 'S2');

insert into raum(raum_id, raum_nr, sem_id) values (1, 'A1', 1);
insert into raum(raum_id, raum_nr, sem_id) values (2, 'A2', 1);
insert into raum(raum_id, raum_nr, sem_id) values (3, 'B1', 1);
insert into raum(raum_id, raum_nr, sem_id) values (4, 'B2', 2);

insert into student(stud_id, stud_name, sem_id) values(1, 'Christian Kranabetter', 1);
insert into student(stud_id, stud_name, sem_id) values(2, 'Tobias Pfeiffer', 1);
insert into student(stud_id, stud_name, sem_id) values(3, 'Fabian Stiefsohn', 2);



select *
from student
inner join semester
    on student.sem_id = semester.sem_id
inner join raum
    on semester.sem_id = raum.sem_id
where student.stud_name = 'Christian Kranabetter';



-- chasm trap

drop table mitarbeiter;
drop table auto;
drop table abteilung;

create table abteilung (
    abt_id integer not null primary key,
    abt_bez varchar2(50) null
);

create table mitarbeiter (
mit_id integer not null primary key,
mit_name varchar2(50) null,
auto_id integer null
);

create table auto (
    auto_id integer not null primary key,
    auto_marke varchar2(50) null,
    abt_id integer null
);

alter table mitarbeiter add foreign key (auto_id) references auto(auto_id);
alter table auto add foreign key (abt_id) references abteilung(abt_id);

insert into abteilung(abt_id, abt_bez) values (1, 'IF');
insert into abteilung(abt_id, abt_bez) values (2, 'EL');

insert into auto(auto_id, auto_marke, abt_id) values (1, 'Volkswagen', 1);
insert into auto(auto_id, auto_marke, abt_id) values (2, 'BMW', 1);
insert into auto(auto_id, auto_marke, abt_id) values (3, 'Mercedes', 2);

insert into mitarbeiter(mit_id, mit_name, auto_id) values(1, 'Christian Kranabetter', 1);
insert into mitarbeiter(mit_id, mit_name, auto_id) values(2, 'Tobias Pfeiffer', 2);
insert into mitarbeiter(mit_id, mit_name, auto_id) values(3, 'Fabian Stiefsohn', 3);
insert into mitarbeiter(mit_id, mit_name, auto_id) values(4, 'Matteo Heindl', null);



select *
from mitarbeiter
inner join auto
    on mitarbeiter.auto_id = auto.auto_id
inner join abteilung
    on auto.abt_id = abteilung.abt_id
where mitarbeiter.mit_name = 'Matteo Heindl';



-- indirektes erschliessen

drop table mitglied;
drop table fest;
drop table feuerwehr;

create table fest (
    fest_id integer not null primary key,
    fest_bez varchar2(50) null,
    feuer_id integer null
);

create table feuerwehr ( 
    feuer_id integer not null primary key,
    feuer_ort varchar2(50) null
);

create table mitglied (
    mit_id integer not null primary key,
    mit_name varchar2(50) null,
    fest_id integer null
);

alter table fest add foreign key (feuer_id) references feuerwehr(feuer_id);
alter table mitglied add foreign key (fest_id) references fest(fest_id);

insert into feuerwehr(feuer_id, feuer_ort) values(1, 'Karlstetten');
insert into feuerwehr(feuer_id, feuer_ort) values(2, 'St. Pölten');
insert into feuerwehr(feuer_id, feuer_ort) values(3, 'Kleinzell');

insert into fest(fest_id, fest_bez, feuer_id) values(1, 'Fest 2018', 1);
insert into fest(fest_id, fest_bez, feuer_id) values(2, 'Fest 2019', 1);
insert into fest(fest_id, fest_bez, feuer_id) values(3, 'Fest 2020', 2);
insert into fest(fest_id, fest_bez, feuer_id) values(4, 'Fest 2021', 3);

insert into mitglied(mit_id, mit_name, fest_id) values(1, 'Christian Kranabetter', 1);
insert into mitglied(mit_id, mit_name, fest_id) values(2, 'Tobias Pfeiffer', 1);
insert into mitglied(mit_id, mit_name, fest_id) values(3, 'Fabian Stiefsohn', 2);
insert into mitglied(mit_id, mit_name, fest_id) values(4, 'Matteo Heindl', 3);
insert into mitglied(mit_id, mit_name, fest_id) values(5, 'Peter Sysel', 4);
insert into mitglied(mit_id, mit_name, fest_id) values(6, 'Josef Gradinger', null);



select *
from mitglied
inner join fest
    on mitglied.fest_id = fest.fest_id
inner join feuerwehr
    on fest.feuer_id = feuerwehr.feuer_id
where mitglied.mit_name = 'Josef Gradinger';
