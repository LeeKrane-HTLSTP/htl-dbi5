-- creating 2 extra users
connect sys/manager12@pdbif1 as sysdba;

create user ifc_15_von identified by mat;
create user ifc_15_nach identified by mat;

-- dropping all tables for better visualization
connect ifc_15/mat@pdbif1;

select * from user_tables;

drop table project cascade constraints;
drop table workson cascade constraints;
drop table wp cascade constraints;
drop table employee cascade constraints;
drop table invoice_head cascade constraints;
drop table invoice_line cascade constraints;
drop table customer cascade constraints;
drop table feuerwehr cascade constraints;
drop table fest cascade constraints;
drop table mitglied cascade constraints;
drop table article cascade constraints;
drop table raum cascade constraints;
drop table abteilung cascade constraints;
drop table auto cascade constraints;
drop table mitarbeiter cascade constraints;
drop table student cascade constraints;
drop table contact cascade constraints;
drop table price cascade constraints;
drop table emp cascade constraints;
drop table e cascade constraints;
drop table renter cascade constraints;
drop table viewing cascade constraints;
drop table kunde cascade constraints;
drop table produkt cascade constraints;
drop table bestellt cascade constraints;
drop table semester cascade constraints;

select * from user_tables;

-- creating test table
drop table diploma_thesis cascade constraints;

create table diploma_thesis (
    dip_id int primary key,
    dip_author varchar2(100),
    dip_title varchar2(100),
    dip_pages int,
    dip_chapters int
);

insert into diploma_thesis values (1, 'Christian Kranabetter', 'Frameworks zur Frontend-Entwicklung', 50, 8);
insert into diploma_thesis values (2, 'Philip Daxböck', 'Künstliche Intelligenz', 55, 5);
insert into diploma_thesis values (3, 'Amelie Zöchling', 'REST', 67, 6);
insert into diploma_thesis values (4, 'Jakob Kammleitner', 'Streamingprotokolle', 62, 6);
insert into diploma_thesis values (5, 'Anja Punz', 'Android', 59, 7);
commit;

select * from diploma_thesis;

-- view roles and privileges
select * from session_privs;
select * from user_role_privs;
select * from user_tab_privs;
select * from user_col_privs;

-- grant privileges to users
grant select on diploma_thesis to ifc_15_von;
grant update(dip_pages, dip_chapters) on diploma_thesis to ifc_15_von;
commit;

-- procedure for sql generation of granting privileges
create or replace procedure generate_grant_privileges_sql (user_from in varchar2, user_to in varchar2) as
    sql_output varchar(1024);
begin
    -- table privileges
    -- note: u'\000A' writes a newline
    for priv in (select table_name, privilege, grantee, grantor from user_tab_privs)
    loop
        if priv.grantee = user_from then
            sql_output := sql_output || 'grant ' || priv.privilege || ' on '
                              || priv.grantor || '.' || priv.table_name || ' to '
                              || user_to || ';' || u'\000A';
        end if;
    end loop;
    -- column privileges
    for priv in (select table_name, column_name, privilege, grantee, grantor from user_col_privs)
    loop
        if priv.grantee = user_from then
            sql_output := sql_output || 'grant ' || priv.privilege || '('
                              || priv.column_name || ') on ' || priv.grantor
                              || '.' || priv.table_name || ' to ' || user_to
                              || ';' || u'\000A';
        end if;
    end loop;
    -- output the collected sql
    dbms_output.put_line(sql_output);
end;

-- activate server output for the procedure to work (on Oracle SQL Plus on Remote Desktop) (if needed)
set serveroutput on size 30000;

-- execute (on Oracle SQL Plus on Remote Desktop)
exec generate_grant_privileges_sql('IFC_15_VON', 'IFC_15_NACH');



grant select on diploma_thesis to ifc_15;
grant update(dip_pages, dip_chapters) on diploma_thesis to ifc_15;
commit;
