-- unten ist pseudo code, der noch in pl/sql umgewandelt werden muss

declare
    type stringarray is varray(255) of varchar2(128);

    tables stringarray;
    tab_columns stringarray;

    sqlquery varchar2(5000);
begin

end;


-- PSEUDO CODE (angelehnt an Java bzw. Javascript code) (\n ist eine neue Zeile)

for (table in (select * from user_tables)) {
    sqlquery += 'drop table ' + table.table_name + ' cascade constraints;\n'
    sqlquery += 'create table ' + table.table_name + '(\n';

    for (column in (select * from user_tab_columns where table_name = table.table_name)) {
        sqlquery += column.column_name + ' ' + column.data_type + ' (' + column.data_length + ')' + ',\n'
    }

    -- hier entfernen von letztem beistrich, danach:

    sqlquery += ');\n'

    for (constraint in (select * from user_constaints where table_name = table.table_name)) {
        sqlquery += 'alter table ' + table.table_name + ' add constraint ' + constraint.constraint_name + ' '

        if (contraint.constraint_type == 'P' {
            sqlquery += 'primary key (' + constraint.index_name + ');\n' -- hier gehört noch bedacht, dass primary keys aus mehreren columns existieren können
        }

        else if (constraint.constraint_type == 'C' {
            sqlquery += 'check (' + constriant.search_condition + ');\n'
        }

        else if (constraint.constraint_type == 'R') {
            select * into consColumn from user_cons_columns where constraint_name = constraint.constraint_name

            sqlquery += 'foreign key (' + consColumn.column_name + ') references ' + consColumn.table_name + '(' + (spaltenname auf den die Referenz zeigt) + ');\n'
        }
    }
}


