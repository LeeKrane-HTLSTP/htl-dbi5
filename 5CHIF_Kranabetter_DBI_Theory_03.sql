connect ifc_15/mat@pdbif1

clear buffer;
clear screen;
set linesize 32000;
set pagesize 2000;

drop table emp cascade constraints;
drop table workson cascade constraints;
drop table project cascade constraints;

create table emp (
                     ssn int primary key,
                     fname varchar2(5),
                     lname varchar2(5),
                     bdate date,
                     address varchar2(5)
);

insert into emp values(1, 'Bert', 'old', '01-jan-01', 'home1');
insert into emp values(2, 'Bert1', 'jung1', '01-jan-10', 'home1');
insert into emp values(3, 'Bert2', 'old2', '01-jan-01', 'home1');
insert into emp values(4, 'Bert3', 'jung2', '01-jan-07', 'home1');

create table project (
                         pnumber int primary key,
                         pname varchar2(5),
                         budget int
);

insert into project values(1, 'p1', 999);
insert into project values(2, 'p2', 666);
insert into project values(3, 'p3', 333);


create table workson (
                         essn int,
                         pno int,
                         hours int,
                         constraint essn_pno  primary key (essn, pno)
);


insert into workson values(1,1,10);
insert into workson values(1,2,10);
insert into workson values(2,1,10);
insert into workson values(2,2,10);
insert into workson values(3,2,10);



alter table workson add constraint fk_workson_emp
    foreign key (essn) references emp(ssn);

alter table workson add constraint fk_workson_project
    foreign key (pno) references project(pnumber);

-- Seite 21

select lname
from emp, workson, project
where  pname = 'p2'
  and pnumber = pno
  and essn = ssn and bdate > '01-jan-01';

-- Achtung!
-- folgende Schritte werden nicht gezeigt in Grafiken
-- kein guter (perfekter) L�sungsansatz

select lname
from (select *
      from project
      where pname = 'p2'),
     workson,
     (select *
      from emp
      where bdate > TO_DATE('01-01-2005', 'DD-MM-YYYY'))
where pnumber = pno
  and essn = ssn;


-- warum: Man muss den Join �ber drei Tables aufbrechen in einen Join mit jeweils 2 Tables mit der where-Klausel
-- Zuerst a mit b verjoinen und dann das Ergebnis mit c!!!



/*
-- weitere Schritte sind nicht wirklich optimal, sondern intuitiv -- eigener Ansatz -- funktioniert jedoch
select lname
from (select pnumber
      from project
      where pname = 'p2') where pnumber = (select pno
                                           from workson where essn = (select ssn
                                                                      from emp
                                                                      where bdate > '01-jan-05'));

-- geht nicht
-- warum?
-- daher zerlegt

select lname
from emp
where ssn = (select essn
             from workson where pno = (select pnumber
                                       from project
                                       where pname = 'p2'));

-- innerer Teil geht

select lname
from emp
where ssn in (select essn
             from workson where pno = (select pnumber
                                       from project
                                       where pname = 'p2'));

-- n�chster schritt


-- geht nicht weil er bdate nicht sieht
select lname
from (
select lname
from emp
where ssn in (select essn
             from workson where pno = (select pnumber
                                       from project
                                       where pname = 'p2')))
where bdate > '01-jan-05';


-- geht

select lname
from (
select *
from emp
where ssn in (select essn
             from workson where pno = (select pnumber
                                       from project
                                       where pname = 'p2')))
where bdate > '01-jan-05';


-- mit join statt where
-- innerer join

select essn
from workson join (select pnumber
                          from project
                          where pname = 'p2')
on pno = pnumber;


select lname
from (
select *
from emp
where ssn in (select essn
from workson join (select pnumber
                          from project
                          where pname = 'p2')
on pno = pnumber))
where bdate > '01-jan-05';


-- letzter Schritt:
-- zerlegen

create table e as
select *
      from emp
      where bdate > '01-jan-05';

create table wp as
select *
from workson join (select pnumber
                          from project
                          where pname = 'p2')
on pno = pnumber;

select * from e;
select * from wp;

select lname
from e join wp
on ssn = essn;

-- daher

select *
from (select *
      from emp
      where bdate > '01-jan-05') inner join
 (select *
from workson join (select pnumber
                          from project
                          where pname = 'p2')
on pno = pnumber)
on ssn = essn;

-- es sollte auffallen, dass das Ergebnis gleich ist dem unteren
-- der Weg ist ein anderer!!!
-- Reihenfolge muss umgedreht werden!!!

*/


-- seite 23
-- schritt 1
-- selection nach unten

select lname
from (select *
      from (select lname, ssn
            from Emp
            WHERE bdate > '01-jan-01'),
           workson
      where essn = ssn),
     (SELECT *
      FROM Project
      WHERE PNAME = 'p2')
where pnumber = pno;

-- seite 24
-- schritt 2
-- restriktiverer Join nach unten

select lname
from (select *
      from (SELECT *
            FROM Project
            WHERE PNAME = 'p2'),
           workson
      where pnumber = pno),
     (select lname, ssn
      from Emp
      WHERE bdate > '01-jan-01')
where essn = ssn;



/*

select lname
from (select *
      from project
      where pname = 'p2'),
      workson,
      (select *
       from emp
       where bdate > TO_DATE('01-01-2005', 'DD-MM-YYYY'))
where pnumber = pno
and essn = ssn;

*/


-- seite 25
-- schritt 3
kartesisches Produkt beseitigen


Select LName
from ((select *
       from project
       where pname = 'p2')
    inner join
    workson
    on pnumber = pno)
         inner join
     (select *
      from Emp
      WHERE bdate > '01-jan-01')
     on essn = ssn;

-- l�sung seite 26