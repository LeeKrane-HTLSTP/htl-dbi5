drop table lieferant cascade constraints;
drop table kategor cascade constraints;
drop table artikel cascade constraints;
drop table bestDtl cascade constraints;
drop table bestell cascade constraints;
drop table kunden cascade constraints;
drop table personal cascade constraints;
drop table firmen cascade constraints;

create table lieferant
(
    lieferNr   int,
    kontktPers varchar2(128),
    strasse    varchar2(128),
    ort        varchar2(128),
    plz        varchar2(128),
    land       varchar2(128),
    telefon    varchar2(128)
);

alter table lieferant
    add constraint pk_lieferant primary key (lieferNr);

insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (1, 'Germaine Smye', 'Havey', 'Wakuya', '987-0121', 'Japan', '449-886-2617');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (2, 'Chickie Kock', 'Fallview', 'Tampa', '33647', 'United States', '813-716-6571');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (3, 'Roarke Masurel', 'Myrtle', 'Yanmenkou', null, 'China', '844-592-7203');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (4, 'Florrie Merrgan', 'Tony', 'Passos', '4990-770', 'Portugal', '356-862-6391');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (5, 'Brita Brettle', 'Eliot', 'Roshal', '140732', 'Russia', '919-156-3634');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (6, 'Niccolo Summerlad', 'Michigan', 'Pokrovskoye', '303181', 'Russia', '408-787-9903');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (7, 'Selie Baudacci', 'Emmet', 'Jurak Lao', null, 'Indonesia', '740-254-6477');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (8, 'Sonja Nouch', 'Anniversary', 'Taibai', null, 'China', '767-162-4948');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (9, 'Amandi Sparkwell', 'Messerschmidt', 'Kuala Belait', null, 'Brunei', '997-505-0324');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (10, 'Yvonne Hockey', 'Lakeland', 'Aktau', null, 'Kazakhstan', '247-131-8723');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (11, 'Lizzy Vezey', 'Rowland', 'Burgkirchen', '5271', 'Austria', '272-990-9043');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (12, 'Dorian Shipman', 'Pine View', 'Kilingi-Nõmme', null, 'Estonia', '719-515-5617');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (13, 'Blake Blitz', 'Rutledge', 'Tanggulangin', null, 'Indonesia', '574-516-4732');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (14, 'Tish Daynter', 'Mayfield', 'Permas', null, 'Indonesia', '278-296-0865');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (15, 'Alley Pau', 'Sugar', 'Vila Chã', '4485-518', 'Portugal', '737-761-9887');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (16, 'Gabie Reede', 'Lukken', 'Warungpeuteuy', null, 'Indonesia', '365-364-1693');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (17, 'Karry Baumler', 'Kings', 'Rudnik', '47-411', 'Poland', '867-193-7832');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (18, 'Lemuel Straughan', 'Autumn Leaf', 'Gurbuki', '368547', 'Russia', '877-496-1281');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (19, 'Amalia Gerrietz', 'Fordem', 'Smimou', null, 'Morocco', '739-283-6385');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (20, 'Naomi Moffett', 'Debra', 'Culianin', '7209', 'Philippines', '839-989-3796');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (21, 'Gerardo Gooding', 'Eagan', 'Sainte-Martine', 'A1C', 'Canada', '585-845-5166');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (22, 'Barry McQuarter', 'Monterey', 'Würzburg', '97078', 'Germany', '738-280-0503');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (23, 'Clarine Kneeland', 'Main', 'Cruz de Pau', '2845-005', 'Portugal', '444-385-4295');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (24, 'Gallard Fetterplace', 'Summit', 'Cairo', null, 'Egypt', '857-527-1885');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (25, 'Jules Strelitzki', 'Starling', 'Gigante', '414009', 'Colombia', '927-675-3370');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (26, 'Lyndel Jagger', 'Pine View', 'Heping', null, 'China', '810-657-6196');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (27, 'Meghan Rupp', 'Badeau', 'Nice', '06102 CEDEX 2', 'France', '223-278-8333');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (28, 'Cahra Forsbey', 'Park Meadow', 'Toutosa', '4635-522', 'Portugal', '791-169-7382');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (29, 'Editha Clifft', 'Claremont', 'Tocoa', null, 'Honduras', '153-475-2839');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (30, 'Keene Junkin', 'Coleman', 'Strängnäs', '645 23', 'Sweden', '769-609-4036');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (31, 'Jack Swetmore', 'Longview', 'Āzādshahr', null, 'Iran', '366-957-6927');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (32, 'Sioux Glassborow', '8th', 'Birnin Kebbi', null, 'Nigeria', '101-163-4762');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (33, 'Koressa Butteris', 'Pearson', 'Sicaya', null, 'Peru', '914-358-3165');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (34, 'Tonnie Enos', 'Marquette', 'Yonghe', null, 'China', '255-572-9456');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (35, 'Tomasine Bourtoumieux', 'Nevada', 'Chiang Khan', '42110', 'Thailand', '567-161-3039');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (36, 'Teodor Sivell', 'Morningstar', 'Borzęta', '32-447', 'Poland', '289-181-6936');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (37, 'Alvan Wemyss', 'Melody', 'Puyuan', null, 'China', '989-243-5304');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (38, 'Hettie Rozenbaum', 'Beilfuss', 'Paratunka', '659642', 'Russia', '485-374-4988');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (39, 'Jess Barme', 'Straubel', 'La Cesira', '5101', 'Argentina', '789-974-0016');
insert into lieferant (lieferNr, kontktPers, strasse, ort, plz, land, telefon) values (40, 'Stephine Stancer', 'Melvin', 'Gorzów Wielkopolski', '66-418', 'Poland', '149-266-0388');
commit;

create table kategor
(
    kategNr   int,
    kategName varchar2(128),
    beschreib varchar2(128)
);

alter table kategor
    add constraint pk_kategor primary key (kategNr);

insert into kategor (kategNr, kategName, beschreib) values (1, 'Tools', 'transition integrated channels');
insert into kategor (kategNr, kategName, beschreib) values (2, 'Games', 'seize intuitive deliverables');
insert into kategor (kategNr, kategName, beschreib) values (3, 'Baby', 'orchestrate collaborative e-services');
insert into kategor (kategNr, kategName, beschreib) values (4, 'Clothing', 'architect strategic vortals');
insert into kategor (kategNr, kategName, beschreib) values (5, 'Music', 'incubate out-of-the-box markets');
insert into kategor (kategNr, kategName, beschreib) values (6, 'Automotive', 'integrate killer relationships');
insert into kategor (kategNr, kategName, beschreib) values (7, 'Games', 'maximize bleeding-edge supply-chains');
insert into kategor (kategNr, kategName, beschreib) values (8, 'Beauty', 'deliver impactful users');
insert into kategor (kategNr, kategName, beschreib) values (9, 'Jewelry', 'transform real-time architectures');
insert into kategor (kategNr, kategName, beschreib) values (10, 'Tools', 'engage cross-media solutions');
insert into kategor (kategNr, kategName, beschreib) values (11, 'Music', 'streamline next-generation web services');
insert into kategor (kategNr, kategName, beschreib) values (12, 'Toys', 'drive granular users');
insert into kategor (kategNr, kategName, beschreib) values (13, 'Baby', 'aggregate ubiquitous e-business');
insert into kategor (kategNr, kategName, beschreib) values (14, 'Movies', 'unleash holistic networks');
insert into kategor (kategNr, kategName, beschreib) values (15, 'Games', 'seize mission-critical vortals');
insert into kategor (kategNr, kategName, beschreib) values (16, 'Games', 'streamline holistic technologies');
insert into kategor (kategNr, kategName, beschreib) values (17, 'Tools', 'implement 24/365 web-readiness');
insert into kategor (kategNr, kategName, beschreib) values (18, 'Movies', 'monetize distributed web services');
insert into kategor (kategNr, kategName, beschreib) values (19, 'Music', 'unleash revolutionary e-markets');
insert into kategor (kategNr, kategName, beschreib) values (20, 'Games', 'grow virtual synergies');
insert into kategor (kategNr, kategName, beschreib) values (21, 'Movies', 'embrace sticky metrics');
insert into kategor (kategNr, kategName, beschreib) values (22, 'Books', 'generate integrated methodologies');
insert into kategor (kategNr, kategName, beschreib) values (23, 'Outdoors', 'disintermediate collaborative infrastructures');
insert into kategor (kategNr, kategName, beschreib) values (24, 'Health', 'optimize synergistic systems');
insert into kategor (kategNr, kategName, beschreib) values (25, 'Movies', 'expedite 24/365 mindshare');
insert into kategor (kategNr, kategName, beschreib) values (26, 'Baby', 'scale 24/7 mindshare');
insert into kategor (kategNr, kategName, beschreib) values (27, 'Home', 'streamline magnetic ROI');
insert into kategor (kategNr, kategName, beschreib) values (28, 'Garden', 'monetize efficient paradigms');
insert into kategor (kategNr, kategName, beschreib) values (29, 'Garden', 'benchmark value-added technologies');
insert into kategor (kategNr, kategName, beschreib) values (30, 'Clothing', 'enhance open-source e-commerce');
insert into kategor (kategNr, kategName, beschreib) values (31, 'Grocery', 'utilize impactful metrics');
insert into kategor (kategNr, kategName, beschreib) values (32, 'Books', 'architect one-to-one communities');
insert into kategor (kategNr, kategName, beschreib) values (33, 'Games', 'engineer holistic action-items');
insert into kategor (kategNr, kategName, beschreib) values (34, 'Health', 'engage viral e-business');
insert into kategor (kategNr, kategName, beschreib) values (35, 'Health', 'evolve proactive users');
insert into kategor (kategNr, kategName, beschreib) values (36, 'Shoes', 'evolve mission-critical infomediaries');
insert into kategor (kategNr, kategName, beschreib) values (37, 'Automotive', 'repurpose out-of-the-box web services');
insert into kategor (kategNr, kategName, beschreib) values (38, 'Movies', 'deliver seamless solutions');
insert into kategor (kategNr, kategName, beschreib) values (39, 'Baby', 'facilitate mission-critical eyeballs');
insert into kategor (kategNr, kategName, beschreib) values (40, 'Shoes', 'exploit revolutionary metrics');
commit;

create table artikel
(
    artikelNr  int,
    lieferNr   int,
    kategNr    int,
    artikel    varchar2(128),
    -- wie liter, flaschen
    lieferEinh varchar2(128),
    einzPreis  number(10, 2),
    lagerBest  int,
    bestEinh   int,
    minBestand int,
    auslaufArt varchar2(128)
);

alter table artikel
    add constraint pk_artikel primary key (artikelNr);
alter table artikel
    add constraint fk_artikel_kategor foreign key (kategNr) references kategor (kategNr);
alter table artikel
    add constraint fk_artikel_lieferant foreign key (lieferNr) references lieferant (lieferNr);

insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (1, 2, 19, 'Schnappes Peppermint - Walker', 'Flaschen', 96.21, 266.59, 1305.89, 936.58, 'Soup - Base Broth Beef');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (2, 39, 39, 'Wine - Red, Cabernet Merlot', 'Stück', 39.49, 516.23, 1716.33, 48.32, 'Coconut - Whole');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (3, 14, 37, 'Puff Pastry - Slab', 'Stück', 19.25, 1652.22, 739.31, 946.82, 'Lamb - Loin Chops');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (4, 15, 35, 'Pepper - Chillies, Crushed', 'Liter', 55.25, 986.53, 1336.83, 445.84, 'Wine - Red, Lurton Merlot De');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (5, 7, 26, 'Appetizer - Smoked Salmon / Dill', 'Liter', 48.16, 1999.18, 604.53, 112.65, 'Tabasco Sauce, 2 Oz');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (6, 36, 35, 'Venison - Ground', 'Liter', 52.37, 868.79, 943.29, 546.16, 'Tortillas - Flour, 10');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (7, 34, 30, 'Sauce - Marinara', 'Stück', 76.26, 90.74, 220.47, 119.23, 'Wine - Vineland Estate Semi - Dry');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (8, 27, 29, 'Butter Sweet', 'Stück', 2.56, 1297.27, 1435.67, 337.17, 'Flour - Rye');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (9, 25, 4, 'Water - San Pellegrino', 'Kilogramm', 98.95, 1565.76, 672.1, 579.26, 'Schnappes - Peach, Walkers');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (10, 3, 9, 'Wine - Chianti Classica Docg', 'Stück', 65.18, 1979.29, 384.77, 109.31, 'Curry Paste - Madras');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (11, 36, 17, 'Fiddlehead - Frozen', 'Flaschen', 29.7, 650.66, 526.82, 563.68, 'Pepper - Pablano');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (12, 25, 32, 'Dikon', 'Stück', 99.22, 1396.09, 808.85, 343.67, 'Pastry - Butterscotch Baked');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (13, 25, 32, 'Tequila Rose Cream Liquor', 'Liter', 71.65, 1977.53, 1516.46, 782.49, 'Cheese - Shred Cheddar / Mozza');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (14, 15, 4, 'Wine - White, French Cross', 'Kilogramm', 6.54, 976.92, 640.24, 842.01, 'Pickerel - Fillets');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (15, 21, 18, 'Water - Tonic', 'Kilogramm', 9.76, 1440.7, 1477.2, 546.0, 'Glass Clear 8 Oz');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (16, 35, 36, 'Sauce - Balsamic Viniagrette', 'Stück', 43.54, 982.97, 1968.34, 617.75, 'Tart Shells - Barquettes, Savory');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (17, 20, 23, 'Crush - Grape, 355 Ml', 'Liter', 23.88, 114.57, 153.47, 766.9, 'Soup - Base Broth Chix');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (18, 15, 31, 'Wine - Red, Cooking', 'Flaschen', 46.79, 285.96, 1857.35, 381.13, 'Beef - Ground Lean Fresh');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (19, 40, 30, 'Yeast - Fresh, Fleischman', 'Flaschen', 72.45, 1100.64, 671.12, 87.16, 'Sausage - Breakfast');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (20, 15, 7, 'Tea - Decaf 1 Cup', 'Liter', 36.25, 511.81, 1800.08, 461.5, 'Tuna - Bluefin');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (21, 26, 3, 'Champagne - Brights, Dry', 'Flaschen', 29.96, 1964.79, 1052.73, 226.75, 'Sea Bass - Fillets');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (22, 2, 6, 'V8 - Berry Blend', 'Stück', 57.87, 1463.39, 441.36, 888.0, 'Wine - Winzer Krems Gruner');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (23, 9, 6, 'Juice - Lagoon Mango', 'Liter', 49.7, 238.54, 1989.24, 628.65, 'Crackers - Melba Toast');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (24, 9, 18, 'Mince Meat - Filling', 'Liter', 4.2, 140.96, 1442.1, 135.79, 'Sprite - 355 Ml');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (25, 15, 27, 'Sugar - Icing', 'Stück', 7.2, 1384.22, 564.1, 951.17, 'Rum - Dark, Bacardi, Black');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (26, 32, 21, 'Wine - Alsace Gewurztraminer', 'Liter', 57.71, 1315.77, 895.81, 870.71, 'Milk - 2%');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (27, 20, 1, 'Split Peas - Yellow, Dry', 'Flaschen', 76.96, 1403.37, 1974.62, 468.84, 'Veal - Nuckle');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (28, 18, 15, 'Salt - Sea', 'Liter', 91.34, 906.0, 1060.06, 399.23, 'Ocean Spray - Kiwi Strawberry');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (29, 25, 27, 'Cheese - Cottage Cheese', 'Stück', 1.75, 515.8, 1378.48, 31.52, 'Bread - English Muffin');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (30, 40, 8, 'Soup - Campbells - Chicken Noodle', 'Kilogramm', 45.69, 28.0, 1711.81, 129.36, 'Wine - Chateauneuf Du Pape');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (31, 21, 9, 'Lettuce - Lambs Mash', 'Liter', 45.4, 1521.59, 1282.45, 308.61, 'Juice - Apple, 1.36l');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (32, 26, 30, 'Sherry - Dry', 'Flaschen', 3.11, 1710.4, 1002.22, 144.51, 'Carbonated Water - Wildberry');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (33, 18, 37, 'Foie Gras', 'Flaschen', 1.06, 1958.18, 1077.93, 53.18, 'Cheese - Ermite Bleu');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (34, 11, 40, 'Wine - Cotes Du Rhone', 'Kilogramm', 93.31, 1356.53, 950.92, 345.08, 'Chocolate - White');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (35, 33, 29, 'Ocean Spray - Ruby Red', 'Flaschen', 4.49, 962.6, 1300.55, 727.16, 'Glass Clear 8 Oz');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (36, 40, 31, 'Salt And Pepper Mix - White', 'Liter', 85.58, 1991.24, 1914.22, 631.66, 'Soup - Beef, Base Mix');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (37, 1, 21, 'Soup - Campbells - Chicken Noodle', 'Stück', 12.67, 1400.5, 1369.01, 674.3, 'Contreau');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (38, 12, 22, 'Lotus Root', 'Kilogramm', 63.17, 327.46, 1496.58, 464.31, 'Wine - Red, Marechal Foch');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (39, 4, 24, 'Lobster - Tail, 3 - 4 Oz', 'Liter', 7.08, 1717.01, 1039.39, 947.89, 'Red Snapper - Fresh, Whole');
insert into artikel (artikelNr, lieferNr, kategNr, artikel, lieferEinh, einzPreis, lagerBest, bestEinh, minBestand, auslaufArt) values (40, 17, 13, 'Wine - Chateau Aqueria Tavel', 'Stück', 48.43, 960.02, 919.78, 258.22, 'Trout Rainbow Whole');
commit;

create table kunden
(
    kundenCode int,
    firma      varchar2(128),
    kontktPers varchar2(128),
    strasse    varchar2(128),
    ort        varchar2(128),
    plz        varchar2(128),
    land       varchar2(128),
    telefon    varchar2(128)
);

alter table kunden
    add constraint pk_kunden primary key (kundenCode);

insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (1, 'Skinix', 'Reade Roggerone', 'Mifflin', 'Xiangquan', null, 'China', '130-596-9758');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (2, 'Kazu', 'Dusty Churchill', 'Pankratz', 'Sumberwaru', null, 'Indonesia', '988-326-6126');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (3, 'Tambee', 'Barbaraanne McAlpin', 'Little Fleur', 'Tamano', '976-0154', 'Japan', '694-492-6193');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (4, 'Mydeo', 'Bradney Dabinett', 'Pawling', 'Nobeoka', '882-0877', 'Japan', '869-477-8168');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (5, 'Leexo', 'Nerti Rhymes', 'Anderson', 'Liuheng', null, 'China', '718-977-5370');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (6, 'Skyba', 'Ceciley Bull', 'Kings', 'Bang Ban', '13250', 'Thailand', '645-149-3296');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (7, 'Devcast', 'Barr Beazley', 'Eliot', 'Anjozorobe', null, 'Madagascar', '889-106-5519');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (8, 'Myworks', 'Michel Lerner', 'Sauthoff', 'Qunsheng', null, 'China', '361-420-3825');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (9, 'Rhynyx', 'Ludwig Simakov', 'Messerschmidt', 'Standerton', '2436', 'South Africa', '574-250-9586');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (10, 'Nlounge', 'Saba Boutellier', 'Sloan', 'Ýdra', null, 'Greece', '757-921-9209');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (11, 'Rooxo', 'Kath Nono', 'Eagle Crest', 'Capela', '4620-297', 'Portugal', '489-838-6187');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (12, 'Gabvine', 'Jeralee Snowling', 'Coolidge', 'Mayrtup', '366319', 'Russia', '462-813-7471');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (13, 'Livetube', 'Woody Maron', 'Straubel', 'Kalianget', null, 'Indonesia', '260-112-0255');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (14, 'Brightbean', 'Cordy Canter', 'Sauthoff', 'Temyasovo', '453663', 'Russia', '946-722-8547');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (15, 'Bubblebox', 'Novelia Mathen', 'Chinook', 'Zapolyarnyy', '184433', 'Russia', '494-143-7783');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (16, 'Browsebug', 'Dianne Abdee', 'Fallview', 'Suntar', '678290', 'Russia', '474-387-6470');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (17, 'Jaxbean', 'Sarah Goodlett', 'Mcguire', 'Hengzhou', null, 'China', '921-445-7592');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (18, 'Realbridge', 'Hendrick Arsey', 'Morning', 'Punta de Piedra', null, 'Venezuela', '947-380-0420');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (19, 'Fivechat', 'Taddeo Click', 'Elgar', 'Uitiuhtuan', null, 'Indonesia', '465-835-6928');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (20, 'Jaxspan', 'Ailene Garth', 'Elgar', 'Emiliano Zapata', '30890', 'Mexico', '361-393-9453');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (21, 'Bluezoom', 'Sherman Ternault', '7th', 'Calape', '6328', 'Philippines', '378-348-0373');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (22, 'Bluezoom', 'Dennison Mauchline', 'Anniversary', 'Galátsi', null, 'Greece', '608-116-2314');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (23, 'Youbridge', 'Ethel McKelvie', 'Eastlawn', 'Delaware', 'M3H', 'Canada', '623-104-2328');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (24, 'Brainverse', 'Morlee Jodlkowski', 'Fair Oaks', 'Trzin', '1236', 'Slovenia', '194-697-8171');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (25, 'Devcast', 'Jeffrey Audus', 'Arkansas', 'Meïganga', null, 'Cameroon', '454-544-2838');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (26, 'Topicware', 'Corly Barhams', 'Granby', 'Tanlin', null, 'China', '242-721-1889');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (27, 'Zoovu', 'Maridel Milhench', 'Shoshone', 'Lubukgadang', null, 'Indonesia', '478-598-2715');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (28, 'Yakidoo', 'Lauren McGlaughn', 'Division', 'Yinjiang', null, 'China', '655-431-4565');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (29, 'Jabbertype', 'Henrietta Zuan', 'Becker', 'Nangerang', null, 'Indonesia', '660-164-4210');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (30, 'Trilia', 'Lovell Legendre', 'Goodland', 'Danirai', null, 'Indonesia', '280-714-4164');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (31, 'Trilith', 'Teador Ubsdall', 'Oxford', 'Werota', null, 'Ethiopia', '927-856-0336');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (32, 'Flipbug', 'Korry Hannabuss', 'Blue Bill Park', 'Krosno', '38-411', 'Poland', '805-604-7479');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (33, 'Quinu', 'Elwira Blasetti', 'Gerald', 'Anding', null, 'China', '128-724-8378');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (34, 'Vipe', 'Lori Anthoine', 'Thackeray', 'Hondo Valle', '11512', 'Dominican Republic', '948-286-0680');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (35, 'Youbridge', 'Barbette Gateland', 'Talisman', 'Qingshan', null, 'China', '830-132-5833');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (36, 'Vinte', 'Shaylah Peealess', 'Ridgeview', 'Yanglang', null, 'China', '912-370-6530');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (37, 'Skinix', 'Ariella Bickardike', 'Mandrake', 'Dongbang', null, 'China', '927-383-5002');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (38, 'Innojam', 'Monty Raatz', 'Nancy', 'Syriam', null, 'Myanmar', '490-676-1627');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (39, 'Wikivu', 'Cyrus Anlay', 'Clarendon', 'Honghualiangzi', null, 'China', '563-925-0632');
insert into kunden (kundenCode, firma, kontktPers, strasse, ort, plz, land, telefon) values (40, 'Quimba', 'Raychel Mouncher', 'Macpherson', 'Jayawangi', null, 'Indonesia', '797-863-6501');
commit;

create table personal
(
    personalNr int,
    nachname   varchar2(128),
    vorname    varchar2(128),
    geburtsDat date,
    einstDat   date,
    strasse    varchar2(128),
    ort        varchar2(128),
    plz        varchar2(128),
    land       varchar2(128),
    telBuero   varchar2(128)
);

alter table personal
    add constraint pk_personal primary key (personalNr);

insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (1, 'Keenlyside', 'Nichol', '22/01/2020', '03/08/2014', 'Dawn', 'Bongandanga', null, 'Democratic Republic of the Congo', '780-224-8408');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (2, 'Loftin', 'Rodolph', '21/02/2013', '24/02/2014', 'Johnson', 'Rochester', '14683', 'United States', '585-200-8504');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (3, 'Kopman', 'Meryl', '28/12/2020', '06/04/2014', 'Division', 'Edosaki', '300-0636', 'Japan', '643-491-7230');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (4, 'Colmer', 'Fletcher', '09/05/2016', '05/01/2016', 'Blaine', 'Zama', '228-0027', 'Japan', '950-427-4862');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (5, 'Danter', 'Kane', '26/03/2016', '15/03/2011', 'Redwing', 'Nisoni', null, 'Indonesia', '885-867-7341');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (6, 'Foley', 'Murielle', '15/03/2013', '06/02/2015', 'Gale', 'Arras', '62010 CEDEX', 'France', '790-122-9492');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (7, 'Quartley', 'Odo', '26/02/2015', '25/11/2013', 'Briar Crest', 'Cigadung', null, 'Indonesia', '992-464-3119');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (8, 'Coils', 'Dari', '04/07/2018', '10/02/2015', 'Sunbrook', 'Junín', null, 'Peru', '452-465-5086');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (9, 'Noell', 'Dewain', '22/04/2018', '02/03/2020', 'Macpherson', 'Energodar', null, 'Ukraine', '262-803-7173');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (10, 'Gibbs', 'Nannette', '08/05/2012', '05/06/2021', 'Porter', 'Adelaide Mail Centre', '5889', 'Australia', '365-764-0428');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (11, 'Garioch', 'Brenden', '23/08/2010', '29/11/2015', 'Sunbrook', 'Koran', null, 'Bosnia and Herzegovina', '574-571-5339');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (12, 'Lavigne', 'Lilith', '06/01/2020', '09/10/2020', 'Holy Cross', 'Alor Setar', '05672', 'Malaysia', '675-693-7487');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (13, 'Motion', 'Gilburt', '23/08/2018', '08/12/2013', 'Nova', 'Porto Alegre', '90000-000', 'Brazil', '166-991-8543');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (14, 'O''Loughlin', 'Clemence', '30/11/2010', '05/10/2013', 'Meadow Valley', 'Gaobu', null, 'China', '281-699-8510');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (15, 'Titterrell', 'Brocky', '19/01/2010', '13/02/2011', 'Mccormick', 'Meybod', null, 'Iran', '211-944-9957');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (16, 'Degg', 'Vicky', '02/09/2021', '14/07/2016', 'Springview', 'Umeå', '904 22', 'Sweden', '647-241-3500');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (17, 'McGeachy', 'Giacomo', '17/08/2021', '10/11/2018', 'Oakridge', 'Laka', null, 'Indonesia', '131-857-5784');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (18, 'Foxen', 'Erik', '13/05/2010', '01/10/2012', 'Autumn Leaf', 'Cleveland', '44105', 'United States', '216-185-4904');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (19, 'Cleveley', 'Rosalinda', '20/11/2014', '23/04/2010', 'Maryland', 'Werang', null, 'Indonesia', '822-692-7423');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (20, 'Tarn', 'Luciano', '01/04/2013', '23/03/2014', 'Browning', 'Sơn Tịnh', null, 'Vietnam', '912-717-3105');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (21, 'MacWhirter', 'Doro', '11/01/2019', '26/08/2012', 'Washington', 'Houston', '77085', 'United States', '713-230-6732');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (22, 'Oswald', 'Jacinta', '13/02/2012', '14/12/2012', 'Derek', 'Tangjiawan', null, 'China', '244-713-1789');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (23, 'Prater', 'Dante', '08/09/2010', '29/09/2021', 'American', 'Põltsamaa', null, 'Estonia', '288-184-9705');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (24, 'Oxborrow', 'Irving', '13/03/2012', '10/12/2010', 'Cardinal', 'Ţāmiyah', null, 'Egypt', '258-508-8456');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (25, 'Grenshiels', 'Ernestine', '30/04/2019', '25/05/2021', 'Chive', 'Lahān', null, 'Nepal', '903-461-2729');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (26, 'Jefford', 'Nicolette', '26/11/2014', '17/07/2011', 'Ryan', 'Taloqan', null, 'Afghanistan', '439-118-6586');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (27, 'Hassin', 'Lina', '25/11/2016', '07/06/2016', 'Northport', 'Carodok', null, 'Indonesia', '833-384-7525');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (28, 'Weighell', 'Steffie', '28/10/2020', '11/01/2012', 'Namekagon', 'Lajarik', null, 'Indonesia', '796-912-2822');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (29, 'Kitchaside', 'Jordana', '22/12/2016', '12/05/2017', 'Summerview', 'Mashan', null, 'China', '342-584-5411');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (30, 'Norwich', 'Robert', '06/02/2022', '23/07/2011', 'Cambridge', 'Polkowice', '59-101', 'Poland', '814-563-5654');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (31, 'Ordish', 'Rip', '03/04/2016', '03/04/2021', 'Alpine', 'Anfu', null, 'China', '978-943-7578');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (32, 'Falcus', 'Pietrek', '27/09/2018', '13/06/2011', 'Esker', 'Punkalaidun', '31901', 'Finland', '424-899-8438');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (33, 'Karlolak', 'Leigh', '21/11/2016', '23/04/2010', 'Northport', 'Shenavan', null, 'Armenia', '865-654-9505');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (34, 'Kingzett', 'Agosto', '01/06/2017', '13/04/2018', 'Mallard', 'Gandra', '4750-402', 'Portugal', '460-263-8013');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (35, 'Marzelo', 'Ailbert', '23/05/2016', '19/07/2020', 'Leroy', 'Kingersheim', '68264 CEDEX', 'France', '255-183-5631');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (36, 'Mealham', 'Salem', '16/10/2010', '05/05/2019', '5th', 'Hedong', null, 'China', '831-104-1485');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (37, 'Milam', 'Cynthie', '25/03/2011', '06/03/2019', 'Gale', 'Minuwangoda', '11550', 'Sri Lanka', '847-690-7908');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (38, 'Schroter', 'Murvyn', '03/10/2020', '25/04/2014', 'Comanche', 'Yangfang', null, 'China', '392-282-6527');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (39, 'Pedrick', 'Hilton', '05/09/2017', '19/02/2010', 'Graceland', 'Sing Buri', '16150', 'Thailand', '313-197-9550');
insert into personal (personalNr, nachname, vorname, geburtsDat, einstDat, strasse, ort, plz, land, telBuero) values (40, 'Whitman', 'Rustin', '20/08/2016', '20/04/2021', 'Maywood', 'Beitou', null, 'China', '481-609-8949');
commit;

create table firmen
(
    firmenNr int,
    firma    varchar2(128)
);

alter table firmen
    add constraint pk_firmen primary key (firmenNr);

insert into firmen (firmenNr, firma) values (1, 'Yombu');
insert into firmen (firmenNr, firma) values (2, 'Bubblebox');
insert into firmen (firmenNr, firma) values (3, 'Jabberstorm');
insert into firmen (firmenNr, firma) values (4, 'Innojam');
insert into firmen (firmenNr, firma) values (5, 'Shufflester');
insert into firmen (firmenNr, firma) values (6, 'Flipstorm');
insert into firmen (firmenNr, firma) values (7, 'Fatz');
insert into firmen (firmenNr, firma) values (8, 'Wikizz');
insert into firmen (firmenNr, firma) values (9, 'Livefish');
insert into firmen (firmenNr, firma) values (10, 'Riffwire');
insert into firmen (firmenNr, firma) values (11, 'Devpulse');
insert into firmen (firmenNr, firma) values (12, 'Mudo');
insert into firmen (firmenNr, firma) values (13, 'Linktype');
insert into firmen (firmenNr, firma) values (14, 'Kare');
insert into firmen (firmenNr, firma) values (15, 'Yotz');
insert into firmen (firmenNr, firma) values (16, 'Trilith');
insert into firmen (firmenNr, firma) values (17, 'Linkbuzz');
insert into firmen (firmenNr, firma) values (18, 'Rhynoodle');
insert into firmen (firmenNr, firma) values (19, 'Avaveo');
insert into firmen (firmenNr, firma) values (20, 'Brightdog');
insert into firmen (firmenNr, firma) values (21, 'Buzzdog');
insert into firmen (firmenNr, firma) values (22, 'Ozu');
insert into firmen (firmenNr, firma) values (23, 'Trudeo');
insert into firmen (firmenNr, firma) values (24, 'Jaloo');
insert into firmen (firmenNr, firma) values (25, 'Midel');
insert into firmen (firmenNr, firma) values (26, 'Skimia');
insert into firmen (firmenNr, firma) values (27, 'Cogilith');
insert into firmen (firmenNr, firma) values (28, 'Devpulse');
insert into firmen (firmenNr, firma) values (29, 'Skimia');
insert into firmen (firmenNr, firma) values (30, 'Feednation');
insert into firmen (firmenNr, firma) values (31, 'Izio');
insert into firmen (firmenNr, firma) values (32, 'Browsedrive');
insert into firmen (firmenNr, firma) values (33, 'Wordpedia');
insert into firmen (firmenNr, firma) values (34, 'Trilia');
insert into firmen (firmenNr, firma) values (35, 'Oodoo');
insert into firmen (firmenNr, firma) values (36, 'Rhycero');
insert into firmen (firmenNr, firma) values (37, 'Devbug');
insert into firmen (firmenNr, firma) values (38, 'Npath');
insert into firmen (firmenNr, firma) values (39, 'Meevee');
insert into firmen (firmenNr, firma) values (40, 'Topicshots');
commit;

create table bestell
(
    bestellNr  int,
    kundenCode int,
    personalNr int,
    empfaenger varchar2(128),
    strasse    varchar2(128),
    ort        varchar2(128),
    plz        varchar2(128),
    zielLand   varchar2(128),
    versndFirm int,
    bestellDat date,
    lieferDat  date,
    versandDat date,
    frachtKost number(10, 2)
);

alter table bestell
    add constraint pk_bestell primary key (bestellNr);
alter table bestell
    add constraint fk_bestell_kunden foreign key (kundenCode) references kunden (kundenCode);
alter table bestell
    add constraint fk_bestell_personal foreign key (personalNr) references personal (personalNr);
alter table bestell
    add constraint fk_bestell_firmen foreign key (versndFirm) references firmen (firmenNr);

insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (1, 6, 31, 'Piggy Edmons', 'Spohn', 'Murcia', '30005', 'Spain', 38, '24/05/2021', '13/06/2021', '25/05/2021', 82.18);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (2, 15, 11, 'Leigha Grutchfield', 'Pepper Wood', 'Quezon', '8715', 'Philippines', 33, '26/03/2016', '14/04/2016', '27/03/2016', 26.89);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (3, 5, 24, 'Blondelle Rooson', 'Center', 'Ashkāsham', null, 'Afghanistan', 14, '23/09/2018', '22/10/2018', '27/09/2018', 25.01);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (4, 38, 20, 'Faye Poston', '8th', 'Barra de São Francisco', '29800-000', 'Brazil', 38, '30/10/2015', '29/11/2015', '03/11/2015', 24.81);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (5, 10, 22, 'Moises Bowen', 'Southridge', 'Tsévié', null, 'Togo', 37, '24/02/2019', '21/03/2019', '25/02/2019', 66.73);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (6, 27, 2, 'Sonnie Jevon', 'Pond', 'Igoumenítsa', null, 'Greece', 34, '20/12/2020', '09/01/2021', '26/12/2020', 78.44);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (7, 9, 24, 'Cammy Oakenfield', 'Delaware', 'Gllogjan', null, 'Kosovo', 18, '23/01/2011', '01/02/2011', '30/01/2011', 65.52);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (8, 35, 34, 'Gwendolin Paff', 'Buell', 'Maddarulug Norte', '3501', 'Philippines', 29, '30/05/2011', '22/06/2011', '31/05/2011', 8.94);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (9, 18, 35, 'Sybille Mongan', 'Dayton', 'Ganhe', null, 'China', 26, '20/12/2013', '04/01/2014', '27/12/2013', 69.02);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (10, 17, 11, 'Jules Frankcom', 'Jackson', 'Wenci', null, 'China', 13, '25/02/2019', '13/03/2019', '27/02/2019', 68.86);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (11, 35, 4, 'Irina Presho', 'Ramsey', 'Zoumi', null, 'Morocco', 12, '25/11/2010', '05/12/2010', '27/11/2010', 53.6);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (12, 24, 40, 'Benedetta Retter', 'Maple Wood', 'Xianju', null, 'China', 36, '02/07/2019', '14/02/2019', '07/02/2019', 57.55);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (13, 27, 38, 'Blondie Emlin', 'Clarendon', 'Chikwawa', null, 'Malawi', 37, '25/11/2014', '24/12/2014', '27/11/2014', 63.96);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (14, 1, 5, 'Berna Franz-Schoninger', 'Manufacturers', 'Tây Hồ', null, 'Vietnam', 10, '05/12/2018', '22/05/2018', '15/05/2018', 93.37);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (15, 37, 40, 'Nicola Wyborn', 'Waubesa', 'Bethlehem', '9706', 'South Africa', 18, '25/05/2011', '21/06/2011', '26/05/2011', 7.48);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (16, 32, 33, 'Mikael Scheffler', 'Cordelia', 'Makadi Bay', null, 'Egypt', 33, '22/10/2020', '17/11/2020', '27/10/2020', 8.62);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (17, 34, 15, 'Theresita Dunnan', 'Scoville', 'Akwanga', null, 'Nigeria', 15, '02/01/2022', '25/02/2022', '04/02/2022', 90.96);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (18, 2, 9, 'Gilberta Kiss', 'Homewood', 'Sapang Buho', '5211', 'Philippines', 7, '11/07/2014', '25/11/2014', '13/11/2014', 9.85);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (19, 17, 23, 'Emelyne Dadge', 'Nevada', 'Birowo', null, 'Indonesia', 17, '23/07/2011', '15/08/2011', '24/07/2011', 2.11);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (20, 15, 14, 'Caroline Dadson', 'Dapin', 'Starobin', null, 'Belarus', 21, '26/03/2019', '06/04/2019', '28/03/2019', 95.21);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (21, 25, 12, 'Gillan Callard', 'Warner', 'Guantang', null, 'China', 24, '05/08/2014', '16/05/2014', '15/05/2014', 57.62);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (22, 26, 8, 'Barrett Middiff', 'Crescent Oaks', 'Bandundu', null, 'Democratic Republic of the Congo', 33, '08/11/2017', '28/08/2017', '14/08/2017', 75.95);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (23, 12, 35, 'Lyn Vosse', 'Sycamore', 'Hispania', '056457', 'Colombia', 24, '03/12/2018', '24/03/2018', '15/03/2018', 75.23);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (24, 25, 30, 'Yankee Adamolli', 'Knutson', 'Karantaba', null, 'Gambia', 15, '13/11/2016', '05/12/2016', '19/11/2016', 5.68);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (25, 33, 19, 'Maury Filde', 'Coolidge', 'Bakulong', '3119', 'Philippines', 10, '24/10/2010', '05/11/2010', '29/10/2010', 8.1);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (26, 3, 4, 'Keefe Varley', 'Burning Wood', 'Kilifi', null, 'Kenya', 8, '28/10/2014', '04/11/2014', '04/11/2014', 10.96);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (27, 37, 5, 'Sebastien Critchlow', 'Wayridge', 'Sumberkertokrajan', null, 'Indonesia', 38, '30/12/2014', '10/01/2015', '31/12/2014', 15.84);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (28, 25, 9, 'Moyra Filkin', 'Old Gate', 'Luleå', '974 36', 'Sweden', 10, '24/02/2021', '11/03/2021', '26/02/2021', 72.29);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (29, 38, 17, 'Mel Suggitt', 'Pond', 'Papelón', null, 'Venezuela', 18, '17/03/2012', '24/03/2012', '22/03/2012', 84.84);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (30, 37, 27, 'Ariadne Leechman', 'Coolidge', 'Soreang', null, 'Indonesia', 15, '03/01/2011', '31/03/2011', '05/03/2011', 13.61);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (31, 38, 9, 'Thaine Plewes', 'Memorial', 'Al Majāridah', null, 'Saudi Arabia', 40, '15/01/2019', '01/02/2019', '16/01/2019', 46.05);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (32, 13, 24, 'Gavan Bartholin', 'Anzinger', 'Vespasiano', '33200-000', 'Brazil', 18, '19/09/2015', '19/10/2015', '26/09/2015', 98.83);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (33, 9, 15, 'Benetta Farlamb', 'Steensland', 'Chitré', null, 'Panama', 22, '09/09/2017', '23/09/2017', '09/09/2017', 76.02);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (34, 23, 25, 'Marina Seiffert', 'Continental', 'San José Acatempa', '22016', 'Guatemala', 2, '18/10/2019', '14/11/2019', '24/10/2019', 15.16);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (35, 20, 18, 'Miguela BURWIN', 'Scott', 'Bensonville', null, 'Liberia', 19, '25/05/2014', '23/06/2014', '30/05/2014', 75.89);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (36, 3, 24, 'Rabi Studdard', 'Mitchell', 'Belene', '5930', 'Bulgaria', 1, '14/04/2021', '07/05/2021', '15/04/2021', 84.25);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (37, 25, 31, 'Gilbertina Stennes', 'Fisk', 'Yupiltepeque', '22006', 'Guatemala', 37, '29/06/2020', '24/07/2020', '30/06/2020', 94.12);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (38, 14, 32, 'Candida Kynson', 'Hoffman', 'Jiujiang', null, 'China', 3, '15/05/2016', '07/06/2016', '21/05/2016', 23.68);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (39, 15, 5, 'Linda Brayne', 'Dapin', 'Kristiansand S', '4638', 'Norway', 27, '03/07/2021', '20/03/2021', '07/03/2021', 32.98);
insert into bestell (bestellNr, kundenCode, personalNr, empfaenger, strasse, ort, plz, zielLand, versndFirm, bestellDat, lieferDat, versandDat, frachtKost) values (40, 8, 17, 'Rudd Casina', 'Anniversary', 'Gubu', null, 'China', 25, '25/04/2020', '25/05/2020', '30/04/2020', 47.09);
commit;

create table bestDtl
(
    bestellNr int,
    artikelNr int,
    anzahl    int,
    rabatt    int
);

alter table bestDtl
    add constraint pk_bestDtl primary key (bestellNr, artikelNr);
alter table bestDtl
    add constraint fk_bestDtl_bestell foreign key(bestellNr) references bestell(bestellNr);
alter table bestDtl
    add constraint fk_bestDtl_artikel foreign key (artikelNr) references artikel(artikelNr);

insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (9, 2, 317, 55);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (13, 17, 340, 9);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (9, 22, 362, 43);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (36, 28, 757, 51);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (22, 25, 514, 69);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (16, 37, 540, 32);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (7, 26, 70, 55);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (4, 14, 854, 42);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (29, 24, 980, 97);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (3, 18, 522, 45);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (17, 27, 440, 39);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (29, 32, 216, 88);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (36, 39, 408, 45);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (6, 35, 852, 10);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (37, 29, 915, 3);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (40, 36, 146, 51);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (34, 24, 410, 39);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (1, 37, 197, 83);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (34, 23, 280, 10);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (24, 5, 31, 35);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (3, 39, 392, 82);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (39, 35, 388, 56);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (7, 40, 508, 62);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (20, 38, 224, 71);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (16, 5, 315, 43);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (19, 6, 472, 38);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (13, 5, 524, 33);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (10, 24, 942, 10);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (18, 8, 247, 89);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (33, 1, 440, 18);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (9, 37, 734, 20);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (31, 31, 265, 54);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (23, 38, 502, 21);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (10, 12, 236, 51);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (22, 13, 893, 64);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (18, 27, 708, 99);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (38, 38, 510, 33);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (8, 33, 514, 54);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (11, 33, 881, 27);
insert into bestDtl (bestellNr, artikelNr, anzahl, rabatt) values (11, 25, 773, 43);
commit;