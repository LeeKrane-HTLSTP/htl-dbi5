connect ifc_15/mat@pdbif1

clear buffer;
clear screen;
set linesize 32000;
set pagesize 2000;

drop table article cascade constraints;
drop table price cascade constraints;
drop table contact cascade constraints;
drop table customer cascade constraints;
drop table employee cascade constraints;
drop table invoice_head cascade constraints;
drop table invoice_line cascade constraints;


-- tables

create table article (
  art_id int,
  art_name varchar2(20) not null unique
);

create table price (
  pr_art_id int,
  pr_from date,
  pr_to date,
  pr_price number(10,2) not null check (pr_price >= 0)
);

create table contact (
  con_id int,
  con_surname varchar2(20) not null,
  con_firstname varchar2(20)
);

create table customer (
  cust_id int,
  cust_creditworthiness int check (cust_creditworthiness in (1, 2, 3, 4))
);

create table employee(
  emp_id int,
  emp_birth_date date not null
);

create table invoice_head (
  inhe_id int,
  inhe_emp_id int,
  inhe_date date not null,
  inhe_cust_id int not null
);

create table invoice_line (
  inli_inhe_id int,
  inli_seq int,
  inli_amount int not null check (inli_amount > 0),
  inli_art_id int not null,
  inli_saleprice number(10, 2) not null check (inli_saleprice >= 0)
);


-- constraints

alter table article add constraint pk_article primary key (art_id);
alter table price add constraint pk_price primary key (pr_art_id, pr_from);
alter table contact add constraint pk_contact primary key (con_id);
alter table customer add constraint pk_customer primary key (cust_id);
alter table employee add constraint pk_employee primary key (emp_id);
alter table invoice_head add constraint pk_invoice_head primary key (inhe_id);
alter table invoice_line add constraint pk_invoice_line primary key (inli_inhe_id, inli_seq);

alter table invoice_line add constraint fk_inli_art foreign key (inli_art_id) references article(art_id);
alter table price add constraint fk_pr_art foreign key (pr_art_id) references article(art_id);
alter table invoice_line add constraint fk_inli_inhe foreign key (inli_inhe_id) references invoice_head(inhe_id);
alter table invoice_head add constraint fk_inhe_cust foreign key (inhe_cust_id) references customer(cust_id);
alter table invoice_head add constraint fk_inhe_emp foreign key (inhe_emp_id) references employee(emp_id);
alter table customer add constraint fk_cust_con foreign key (cust_id) references contact(con_id);
alter table employee add constraint fk_emp_con foreign key (emp_id) references contact(con_id);


-- testdaten

insert into contact (con_id, con_surname, con_firstname) values (1, 'Stayte', 'Hew');
insert into employee (emp_id, emp_birth_date) values (1, to_date('1995-08-29', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (2, 'Barkworth', 'Tabbi');
insert into employee (emp_id, emp_birth_date) values (2, to_date('1999-01-12', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (3, 'Haugen', 'Ezmeralda');
insert into employee (emp_id, emp_birth_date) values (3, to_date('1988-07-22', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (4, 'McWhorter', 'Cherish');
insert into employee (emp_id, emp_birth_date) values (4, to_date('1984-12-07', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (5, 'Meecher', 'Pate');
insert into employee (emp_id, emp_birth_date) values (5, to_date('1988-02-08', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (6, 'Sheaf', 'Nataline');
insert into employee (emp_id, emp_birth_date) values (6, to_date('1987-03-07', 'YYYY-MM-DD'));
commit;
insert into contact (con_id, con_surname, con_firstname) values (7, 'Lejeune', 'Florida');
insert into customer (cust_id, cust_creditworthiness) values (7, 4);
commit;
insert into contact (con_id, con_surname, con_firstname) values (8, 'Sayers', 'Kora');
insert into customer (cust_id, cust_creditworthiness) values (8, 2);
commit;
insert into contact (con_id, con_surname, con_firstname) values (9, 'Piller', 'Zorine');
insert into customer (cust_id, cust_creditworthiness) values (9, 2);
commit;
insert into contact (con_id, con_surname, con_firstname) values (10, 'Graves', 'Gerianne');
insert into customer (cust_id, cust_creditworthiness) values (10, 3);
commit;
insert into contact (con_id, con_surname, con_firstname) values (11, 'Gundrey', 'Harlie');
insert into customer (cust_id, cust_creditworthiness) values (11, 2);
commit;
insert into contact (con_id, con_surname, con_firstname) values (12, 'Swarbrick', 'Rivi');
insert into customer (cust_id, cust_creditworthiness) values (12, 1);
commit;

insert into article (art_id, art_name) values (1, 'Dextromethorphan');
commit;
insert into article (art_id, art_name) values (2, 'Bupropion');
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (2, to_date('2021-06-25', 'YYYY-MM-DD'), to_date('2028-12-28', 'YYYY-MM-DD'), 49.29);
commit;
insert into article (art_id, art_name) values (3, 'Bisacodyl');
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (3, to_date('2021-01-01', 'YYYY-MM-DD'), to_date('2021-06-21', 'YYYY-MM-DD'), 94.99);
commit;
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (3, to_date('2021-06-21', 'YYYY-MM-DD'), to_date('2028-09-24', 'YYYY-MM-DD'), 47.59);
commit;
insert into article (art_id, art_name) values (4, 'Amlodipine');
commit;
insert into article (art_id, art_name) values (5, 'Ioxilan');
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (5, to_date('2021-06-28', 'YYYY-MM-DD'), to_date('2028-09-24', 'YYYY-MM-DD'), 59.59);
commit;
insert into article (art_id, art_name) values (6, 'glucarpidase');
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (6, to_date('2021-04-23', 'YYYY-MM-DD'), to_date('2021-07-13', 'YYYY-MM-DD'), 74.29);
commit;
insert into price (pr_art_id, pr_from, pr_to, pr_price) values (6, to_date('2021-07-13', 'YYYY-MM-DD'), to_date('2028-10-02', 'YYYY-MM-DD'), 67.44);
commit;

insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (1, 2, to_date('2021-04-05', 'YYYY-MM-DD'), 8);
commit;
insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (2, 3, to_date('2021-07-05', 'YYYY-MM-DD'), 9);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (2, 1, 7, 2, 123.91);
commit;
insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (3, 3, to_date('2021-08-21', 'YYYY-MM-DD'), 9);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (3, 2, 5, 3, 47.1);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (3, 3, 6, 3, 67.82);
commit;
insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (4, 5, to_date('2021-08-29', 'YYYY-MM-DD'), 11);
commit;
insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (5, 6, to_date('2021-09-17', 'YYYY-MM-DD'), 12);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (5, 4, 13, 5, 121.71);
commit;
insert into invoice_head (inhe_id, inhe_emp_id, inhe_date, inhe_cust_id) values (6, 6, to_date('2021-10-04', 'YYYY-MM-DD'), 12);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (6, 5, 25, 6, 67.19);
insert into invoice_line (inli_inhe_id, inli_seq, inli_amount, inli_art_id, inli_saleprice) values (6, 6, 9, 6, 27.67);
commit;


-- art_curr_pr view

create or replace view art_curr_pr as
-- todo


-- todo: transaktionen? (historisieren)

-- select SYS_CONTEXT ('USERENV', 'SESSION_USER'), SYSDATE from dual;
-- select 4+1 from dual;
-- select * from nls_session_parameters;
