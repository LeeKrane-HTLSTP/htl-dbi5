drop table kunde cascade constraints;
drop table produkt cascade constraints;
drop table bestellt cascade constraints;

create table kunde (
                       knr int primary key,
                       name varchar2(80),
                       adresse varchar2(80),
                       region varchar2(80),
                       saldo int
);

insert into kunde values (201, 'Klein', 'Lilienthal', 'Bremen', 200000);
insert into kunde values (337, 'Horn', 'Dieburg', 'Rhein-Main', 100000);
insert into kunde values (444, 'Berger', 'München', 'München', 300000);
insert into kunde values (108, 'Weiss', 'Würzburg', 'Unterfranken', 500000);

create table produkt (
                         pnr int primary key,
                         bezeichnung varchar2(80),
                         anzahl int,
                         preis int
);

insert into produkt values (12, 'BMW 318i', 10, 40000);
insert into produkt values (4, 'Golf 5', 40, 25000);
insert into produkt values (330, 'Fiat Uno', 5, 18000);
insert into produkt values (98, 'Ferrari 380', 1, 180000);
insert into produkt values (14, 'Open Corsa', 14, 17000);

create table bestellt (
                          bnr int primary key,
                          datum date,
                          knr int references kunde(knr),
                          pnr int references produkt(pnr)
);

insert into bestellt values (221, to_date('10.05.2004', 'DD.MM.YYYY'), 201, 12);
insert into bestellt values (312, to_date('11.05.2004', 'DD.MM.YYYY'), 201, 4);
insert into bestellt values (401, to_date('20.05.2004', 'DD.MM.YYYY'), 337, 330);
insert into bestellt values (456, to_date('13.05.2004', 'DD.MM.YYYY'), 444, 330);
insert into bestellt values (458, to_date('14.05.2004', 'DD.MM.YYYY'), 444, 98);



-- original selection
select Name
from Kunde k, Bestellt b, Produkt p
where b.KNr = k.KNr
  and b.PNr = p.PNr
  and Bezeichnung = 'Fiat Uno'
  and Saldo >= 300.000;



-- optimized selection
select kb.name
from
    (select name, pnr
     from
         (select name, knr, saldo
          from kunde
          where saldo >= 300000) k
             inner join
         (select knr, pnr
          from bestellt) b
         on b.knr = k.knr) kb
        inner join
    (select pnr
     from produkt
     where bezeichnung = 'Fiat Uno') p
    on kb.pnr = p.pnr;
